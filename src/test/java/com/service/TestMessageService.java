package com.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.client.RestTemplate;

import com.config.ConfigurationEndPoint;
import com.domain.Request;
import com.domain.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({ "javax.management.*" })
@PrepareForTest({ MessageService.class })
public class TestMessageService {
	
	@InjectMocks
	private MessageService mockMessageService;
	
	@Mock
	private ConfigurationEndPoint configurationEndPoint;
	
	@Before
	public void setup() throws Exception {
		PowerMockito.mockStatic(RestTemplate.class);
	}
	
	@Test
	public void test_service_addSlackNoti_success() throws Exception {
		Request request = new Request();
		request.setChannel("#room1");
		request.setText("Hello");
		String ans = "ok";
		String url = "https://hooks.slack.com/services/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";
		
		RestTemplate restTemplate = mock(RestTemplate.class);
//		RestTemplate restTemplate = new RestTemplate();
		PowerMockito.whenNew(RestTemplate.class).withNoArguments().thenReturn(restTemplate);
		
		PowerMockito.when(configurationEndPoint.getSlackNotiEndPoint()).thenReturn(url);
		
		PowerMockito.when(restTemplate.postForObject(url, request, String.class)).thenReturn(ans);
		
		Response response = mockMessageService.addSlackNoti(request);
		
		assertEquals("0", response.getResponse_code());
		assertEquals("SUCCESS", response.getResponse_msg());
	}
}