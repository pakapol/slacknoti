package com.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import com.domain.Request;
import com.domain.Response;
import com.service.MessageService;

@RunWith(PowerMockRunner.class)
public class TestSlackController {
	
	@Mock
    private MessageService messageService;
	
	@InjectMocks
	private SlackController slackController;
	
	@Test
	public void test_addMessage_SUCCESS() {
		//Given
		Request request = new Request();
		request.setChannel("#room1");
		request.setText("Hello");
		Response resp = new Response();
		resp.setResponse_code("0");
		resp.setResponse_msg("SUCCESS");
		//When
		when(messageService.addSlackNoti(request)).thenReturn(resp);
		Response respon = slackController.addMessage(request);
		//Then
		assertEquals("0", respon.getResponse_code());
		assertEquals("SUCCESS", respon.getResponse_msg());
	}

}
