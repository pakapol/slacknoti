package com.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.config.ConfigurationEndPoint;
import com.controller.SlackController;
import com.domain.Request;
import com.domain.Response;

@Service
public class MessageService {
	@Autowired
	private ConfigurationEndPoint configurationEndPoint;
	
	private Logger logger = LogManager.getLogger(SlackController.class);
	
	@ResponseBody
	public Response addSlackNoti(Request slackRequest) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			String endPoint = configurationEndPoint.getSlackNotiEndPoint();
			String response = restTemplate.postForObject(endPoint, slackRequest, String.class);
			
			Response buildResponse = new Response();
			logger.info(response);
			if (response.equalsIgnoreCase("ok")) {
				buildResponse.setResponse_code("0");
				buildResponse.setResponse_msg("SUCCESS");
			} else {
				buildResponse.setResponse_code("0000");
				buildResponse.setResponse_msg("FAIL");
			}
			logger.info("reponse code : " + buildResponse.getResponse_code());
			logger.info("reponse message : " + buildResponse.getResponse_msg());
			return buildResponse;
		} catch (Exception e) {
			throw e;
		}
    }
	
}
