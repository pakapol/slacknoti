package com.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationEndPoint {
	@Value("${slack.notification.endPoint}")
	private String slackNotiEndPoint;
	
	@Value("${ascendmoney.notification.endPoint}")
	private String ascendmoneyNotiEndPoint;

	public String getAscendmoneyNotiEndPoint() {
		return ascendmoneyNotiEndPoint;
	}

	public String getSlackNotiEndPoint() {
		return slackNotiEndPoint;
	}
}
