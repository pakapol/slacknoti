package com.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.domain.Request;
import com.domain.Response;
import com.service.MessageService;

@RestController
public class SlackController {
	@Autowired
    private MessageService messageService;
	
	private Logger logger = LogManager.getLogger(SlackController.class);
	
	@PostMapping(value = "/v1/addslack"  
			,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE 
			,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	public Response addMessage(@RequestBody Request request){
		logger.info("Start");
		logger.info("request message : " + request.getText());
		System.out.println(request.toString());
		return messageService.addSlackNoti(request);
	}
}
