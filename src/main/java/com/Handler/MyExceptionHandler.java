package com.Handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.controller.SlackController;
import com.domain.Response;

@ControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {
	private Logger logger = LogManager.getLogger(SlackController.class);
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	  @ExceptionHandler({Exception.class})
	  public ResponseEntity<Object> badRequest(HttpServletRequest req, Exception exception) {
		Response buildResponse = new Response();
		buildResponse.setResponse_code("400");
		buildResponse.setResponse_msg("can't send message");
		ResponseEntity<Object> res = new ResponseEntity<Object>(buildResponse,HttpStatus.BAD_REQUEST);
		logger.info("reponse code : " + buildResponse.getResponse_code());
		logger.info("reponse message : " + buildResponse.getResponse_msg());
	    return res;
	  }

}
